package net.kodekitten;

import junit.framework.TestCase;
import net.kodekitten.enums.HandResult;

public class HandTest extends TestCase {

    public void testGetHandResult() throws Exception {
        Hand hand = new Hand();
        hand.addCardToHand(new Card("TS"));
        hand.addCardToHand(new Card("JS"));
        hand.addCardToHand(new Card("QS"));
        hand.addCardToHand(new Card("KS"));
        hand.addCardToHand(new Card("AS"));

        assertEquals(hand.getHandResult(), HandResult.ROYAL_FLUSH);

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2H"));
        hand.addCardToHand(new Card("2D"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("5S"));

        assertEquals(hand.getHandResult(), HandResult.FOUR_OF_A_KIND);


        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2H"));
        hand.addCardToHand(new Card("5C"));
        hand.addCardToHand(new Card("5D"));
        hand.addCardToHand(new Card("5S"));

        assertEquals(hand.getHandResult(), HandResult.FULL_HOUSE);

        hand.clear();
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("4C"));
        hand.addCardToHand(new Card("5C"));
        hand.addCardToHand(new Card("6C"));
        hand.addCardToHand(new Card("7C"));
        assertEquals(hand.getHandResult(), HandResult.STRAIGHT_FLUSH);


        hand.clear();
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("8C"));
        hand.addCardToHand(new Card("5C"));
        hand.addCardToHand(new Card("6C"));
        hand.addCardToHand(new Card("7C"));
        assertEquals(hand.getHandResult(), HandResult.FLUSH);


        hand.clear();
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("4C"));
        hand.addCardToHand(new Card("5D"));
        hand.addCardToHand(new Card("6H"));
        hand.addCardToHand(new Card("7C"));
        assertEquals(hand.getHandResult(), HandResult.STRAIGHT);

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("KS"));
        hand.addCardToHand(new Card("KD"));
        hand.addCardToHand(new Card("KS"));
        hand.addCardToHand(new Card("TH"));
        assertEquals(hand.getHandResult(), HandResult.THREE_OF_A_KIND);

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("4D"));
        hand.addCardToHand(new Card("4S"));
        hand.addCardToHand(new Card("TH"));
        assertEquals(hand.getHandResult(), HandResult.TWO_PAIR);

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("QD"));
        hand.addCardToHand(new Card("3S"));
        hand.addCardToHand(new Card("TH"));
        assertEquals(hand.getHandResult(), HandResult.ONE_PAIR);


    }

    public void testIsRoyalFlush() throws Exception {
        Hand hand = new Hand();
        hand.addCardToHand(new Card("TS"));
        hand.addCardToHand(new Card("JS"));
        hand.addCardToHand(new Card("QS"));
        hand.addCardToHand(new Card("KS"));
        hand.addCardToHand(new Card("AS"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertTrue(hand.isRoyalFlush());

        hand.clear();
        hand.addCardToHand(new Card("6S"));
        hand.addCardToHand(new Card("7S"));
        hand.addCardToHand(new Card("8S"));
        hand.addCardToHand(new Card("9S"));
        hand.addCardToHand(new Card("TS"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertFalse(hand.isRoyalFlush());

    }

    public void testIsFours() throws Exception {
        Hand hand = new Hand();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2H"));
        hand.addCardToHand(new Card("2D"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("5S"));

        hand.buildCardsByValue();

        assertTrue(hand.isFours());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("4C"));
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("6H"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();

        assertFalse(hand.isFours());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("6H"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();

        assertFalse(hand.isFours());


    }

    public void testIsFullHouse() throws Exception {
        Hand hand = new Hand();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2H"));
        hand.addCardToHand(new Card("5C"));
        hand.addCardToHand(new Card("5D"));
        hand.addCardToHand(new Card("5S"));

        hand.buildCardsByValue();

        assertTrue(hand.isFullHouse());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("4C"));
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("6H"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();

        assertFalse(hand.isFullHouse());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("6H"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();

        assertFalse(hand.isFullHouse());

    }

    public void testIsFlush() throws Exception {

        Hand hand = new Hand();
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("4C"));
        hand.addCardToHand(new Card("5C"));
        hand.addCardToHand(new Card("8C"));
        hand.addCardToHand(new Card("7C"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertTrue(hand.isFlush());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("4C"));
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("6H"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertFalse(hand.isFlush());


        hand.clear();
        hand.addCardToHand(new Card("9C"));
        hand.addCardToHand(new Card("JC"));
        hand.addCardToHand(new Card("KC"));
        hand.addCardToHand(new Card("QC"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertTrue(hand.isFlush());


    }

    public void testIsStraight() throws Exception {
        Hand hand = new Hand();
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("4D"));
        hand.addCardToHand(new Card("5H"));
        hand.addCardToHand(new Card("6C"));
        hand.addCardToHand(new Card("7C"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertTrue(hand.isStraight());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("4C"));
        hand.addCardToHand(new Card("3C"));
        hand.addCardToHand(new Card("6H"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertFalse(hand.isStraight());


        hand.clear();
        hand.addCardToHand(new Card("9C"));
        hand.addCardToHand(new Card("JC"));
        hand.addCardToHand(new Card("KC"));
        hand.addCardToHand(new Card("QC"));
        hand.addCardToHand(new Card("TC"));

        hand.buildCardsByValue();
        hand.buildCardsBySuit();

        assertTrue(hand.isStraight());

    }

    public void testIsThrees() throws Exception {
        Hand hand = new Hand();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("2D"));
        hand.addCardToHand(new Card("3S"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertTrue(hand.isThrees());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("3H"));
        hand.addCardToHand(new Card("6S"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertFalse(hand.isThrees());


    }

    public void testIsTwoPairs() throws Exception {

        Hand hand = new Hand();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("3H"));
        hand.addCardToHand(new Card("3S"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertTrue(hand.isTwoPairs());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("3H"));
        hand.addCardToHand(new Card("6S"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertFalse(hand.isTwoPairs());


        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("2H"));
        hand.addCardToHand(new Card("2D"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertFalse(hand.isTwoPairs());


    }

    public void testIsPair() throws Exception {
        Hand hand = new Hand();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("3H"));
        hand.addCardToHand(new Card("6S"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertTrue(hand.isPair());

        hand.clear();
        hand.addCardToHand(new Card("AC"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("3H"));
        hand.addCardToHand(new Card("6S"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertFalse(hand.isPair());

        hand.clear();
        hand.addCardToHand(new Card("2C"));
        hand.addCardToHand(new Card("2S"));
        hand.addCardToHand(new Card("2H"));
        hand.addCardToHand(new Card("6S"));
        hand.addCardToHand(new Card("TH"));

        hand.buildCardsByValue();

        assertFalse(hand.isPair());

    }
}