package net.kodekitten.enums;

/**
 * Created by kodekitten on 7/14/14.
 */
public enum Suit {
    CLUBS,
    SPADES,
    HEARTS,
    DIAMONDS
}
