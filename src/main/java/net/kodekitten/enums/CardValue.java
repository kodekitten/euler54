package net.kodekitten.enums;

/**
 * Created by kodekitten on 7/14/14.
 */
public enum CardValue {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE

}
