package net.kodekitten;

import net.kodekitten.enums.CardValue;
import net.kodekitten.enums.HandResult;
import net.kodekitten.enums.Suit;

import java.util.*;

/**
 * Created by kodekitten on 7/14/14.
 */
public class Hand {
    private List<Card> cards;
    private Map<Suit, List<Card>> cardsBySuit;
    private Map<CardValue, List<Card>> cardsByValue;

    private CardComparator cardComparator = new CardComparator();

    public Hand() {
        cards = new ArrayList<Card>();
    }

    public Hand(List<Card> cards) {
        this.cards = cards;
    }

    public void addCardToHand(Card c) {
        cards.add(c);
    }

    public CardValue getHighestCardValue() {
        List<CardValue> cardValues = new ArrayList<CardValue>(cardsByValue.keySet());
        Collections.sort(cardValues);
        Collections.reverse(cardValues);
        return cardValues.get(0);

    }


    public void clear() {
        cards.clear();
    }

    public HandResult getHandResult() {
        //generate maps
        buildCardsBySuit();
        buildCardsByValue();


        if (isRoyalFlush())
            return HandResult.ROYAL_FLUSH;
        else if (isStraightFlush())
            return HandResult.STRAIGHT_FLUSH;
        else if (isFours())
            return HandResult.FOUR_OF_A_KIND;
        else if (isFullHouse())
            return HandResult.FULL_HOUSE;
        else if (isFlush())
            return HandResult.FLUSH;
        else if (isStraight())
            return HandResult.STRAIGHT;
        else if (isThrees())
            return HandResult.THREE_OF_A_KIND;
        else if (isTwoPairs())
            return HandResult.TWO_PAIR;
        else if (isPair())
            return HandResult.ONE_PAIR;

        return HandResult.HIGH_CARD;

    }


    /**
     * true if cards represent Royal Flush (T,J,Q,K,A in one suit)
     */
    boolean isRoyalFlush() {
        if (isStraight() && cardsBySuit.keySet().size() == 1) {
            for (Suit suit : cardsBySuit.keySet()) {
                List<Card> cardList = cardsBySuit.get(suit);
                Collections.sort(cardList, cardComparator);
                if (cardList.get(0).getValue().equals(CardValue.TEN) &&
                        cardList.get(1).getValue().equals(CardValue.JACK) &&
                        cardList.get(2).getValue().equals(CardValue.QUEEN) &&
                        cardList.get(3).getValue().equals(CardValue.KING) &&
                        cardList.get(4).getValue().equals(CardValue.ACE))
                    return true;

            }
        }
        return false;

    }

    /**
     * true if Straight Flush (all in one suit and in order)
     */
    private boolean isStraightFlush() {

        return isFlush() && isStraight();
    }


    /**
     * true if four cards of same value are present
     */
    boolean isFours() {
        for (CardValue cardValue : cardsByValue.keySet()) {
            List<Card> cardList = cardsByValue.get(cardValue);
            if (cardList.size() == 4)
                return true;

        }
        return false;
    }

    /**
     * true if 3 cards of same value and 2 cards of same value are present
     */
    boolean isFullHouse() {
        return isPair() && isThrees();
    }

    /**
     * true if any 5 cards of same suit
     */
    boolean isFlush() {
        for (Suit suit : cardsBySuit.keySet()) {
            List<Card> cardList = cardsBySuit.get(suit);
            if (cardList.size() == 5)
                return true;

        }
        return false;
    }

    /**
     * true if any 5 cards in correct order
     */
    boolean isStraight() {

        List<CardValue> cardValues = new ArrayList<CardValue>(cardsByValue.keySet());
        Collections.sort(cardValues);

        if (cardValues.size() > 4) {
            int previous = cardValues.get(0).ordinal();
            int inOrder = 0;
            for (int i = 1; i < cardValues.size(); i++) {
                if (cardValues.get(i).ordinal() == previous + 1) {
                    inOrder++;

                } else
                    inOrder = 0;
                previous = cardValues.get(i).ordinal();
            }
            if (inOrder == 4)
                return true;

        }


        return false;

    }

    /**
     * true if 3 cards are the same value
     */
    boolean isThrees() {
        for (CardValue cardValue : cardsByValue.keySet()) {
            List<Card> cardList = cardsByValue.get(cardValue);
            if (cardList.size() == 3)
                return true;

        }
        return false;
    }

    /**
     * true if 2 x 2 cards are the same value
     */
    boolean isTwoPairs() {
        int paircount = 0;
        for (CardValue cardValue : cardsByValue.keySet()) {
            List<Card> cardList = cardsByValue.get(cardValue);
            if (cardList.size() == 2)
                paircount++;

        }

        if (paircount == 2)
            return true;
        return false;
    }

    /**
     * true if 2 cards are the same value
     */
    boolean isPair() {
        for (CardValue cardValue : cardsByValue.keySet()) {
            List<Card> cardList = cardsByValue.get(cardValue);
            if (cardList.size() == 2)
                return true;

        }
        return false;

    }


    void buildCardsBySuit() {
        cardsBySuit = new HashMap<Suit, List<Card>>();
        for (Card c : cards) {
            if (!cardsBySuit.containsKey(c.getSuit())) {
                cardsBySuit.put(c.getSuit(), new ArrayList<Card>());
            }
            List<Card> cardList = cardsBySuit.get(c.getSuit());
            cardList.add(c);
            Collections.sort(cardList, cardComparator);
        }

    }

    void buildCardsByValue() {
        cardsByValue = new HashMap<CardValue, List<Card>>();
        for (Card c : cards) {
            if (!cardsByValue.containsKey(c.getValue())) {
                cardsByValue.put(c.getValue(), new ArrayList<Card>());
            }
            List<Card> cardList = cardsByValue.get(c.getValue());
            cardList.add(c);
            Collections.sort(cardList, cardComparator);

        }
    }

    private class CardComparator implements Comparator<Card> {

        @Override
        public int compare(Card o1, Card o2) {
            return o1.compareTo(o2);
        }
    }

}
