package net.kodekitten;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        int player1Wins = 0;
        int totalHands = 0;
        int tiebreakers =0;
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(args[0]));
        } catch (FileNotFoundException e) {
            System.out.println("Unable to find " + args[0]);
        }

        try {
            Hand player1 = new Hand();
            Hand player2 = new Hand();

            String line = reader.readLine();
            while (line != null) {
                //clear the players
                player1.clear();
                player2.clear();

                //parse the hands
                String[] cards = line.split(" ");
                for (int i = 0; i < cards.length; i++) {
                    if (i < 5)
                        player1.addCardToHand(new Card(cards[i]));
                    else
                        player2.addCardToHand(new Card(cards[i]));
                }


                //play the hand

                if (player1.getHandResult().ordinal() > player2.getHandResult().ordinal())
                    player1Wins++;
                if(player1.getHandResult().equals(player2.getHandResult())){
                    tiebreakers++;
                    if(player1.getHighestCardValue().ordinal()>player2.getHighestCardValue().ordinal())
                        player1Wins++;
                }



                //increment total hands played
                totalHands++;


                line = reader.readLine();

            }

            reader.close();

            System.out.println("Player 1 wins " +player1Wins + " out of " + totalHands);
            System.out.println("Number of tiebreakers required: " + tiebreakers);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
