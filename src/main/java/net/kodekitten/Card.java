package net.kodekitten;

import net.kodekitten.enums.CardValue;
import net.kodekitten.enums.Suit;

import java.util.Objects;

/**
 * Created by kodekitten on 7/14/14.
 */
public class Card implements Comparable{
    private CardValue value;
    private Suit suit;

    public CardValue getValue() {
        return value;
    }



    public Suit getSuit() {
        return suit;
    }



    public Card(String s) {
        char[] chars = s.toCharArray();

        switch (chars[1]) {
            case 'C':
                suit = Suit.CLUBS;
                break;
            case 'D':
                suit = Suit.DIAMONDS;
                break;
            case 'H':
                suit = Suit.HEARTS;
                break;
            case 'S':
                suit = Suit.SPADES;
                break;

        }

        switch (chars[0]) {
            case '2':
                value = CardValue.TWO;
                break;
            case '3':
                value = CardValue.THREE;
                break;
            case '4':
                value = CardValue.FOUR;
                break;
            case '5':
                value = CardValue.FIVE;
                break;
            case '6':
                value = CardValue.SIX;
                break;
            case '7':
                value = CardValue.SEVEN;
                break;
            case '8':
                value = CardValue.EIGHT;
                break;
            case '9':
                value = CardValue.NINE;
                break;
            case 'T':
                value = CardValue.TEN;
                break;
            case 'J':
                value = CardValue.JACK;
                break;
            case 'Q':
                value = CardValue.QUEEN;
                break;
            case 'K':
                value = CardValue.KING;
                break;
            case 'A':
                value = CardValue.ACE;
                break;
        }

    }

    @Override
    public int compareTo(Object o) {
        Card c = (Card) o;

        if(this.getValue().ordinal()> c.getValue().ordinal())
            return 1;
        else if(this.getValue().ordinal()< c.getValue().ordinal())
            return -1;
        return 0;
    }
}
